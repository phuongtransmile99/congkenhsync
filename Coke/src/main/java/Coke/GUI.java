package Coke;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.google.api.client.util.DateTime;

import model.FileItem;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTabbedPane;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextPane;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField directText;
	private JTable table, historyTable;
	private JTextField linkDriveText;
	private JTextField localFolderText;
	private String url = "";
	private CardLayout layout;
	Quickstart drive = new Quickstart();
	GoogleSheet sheet = new GoogleSheet();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public GUI() throws GeneralSecurityException, IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 936, 635);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setBounds(0, 0, 133, 596);
		contentPane.add(panel);
		panel.setLayout(null);

		JButton historyFile = new JButton("History");

		historyFile.setForeground(Color.WHITE);
		historyFile.setBackground(Color.BLACK);
		historyFile.setBounds(21, 182, 89, 23);
		panel.add(historyFile);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(133, 0, 787, 596);
		contentPane.add(panel_1);
		panel_1.setLayout(new CardLayout(0, 0));

		layout = (CardLayout) panel_1.getLayout();

		JPanel mangeCard = new JPanel();
		mangeCard.setBackground(Color.PINK);
		panel_1.add(mangeCard, "name_40465715806500");
		mangeCard.setLayout(null);

		directText = new JTextField();
		directText.setBounds(13, 554, 326, 20);
		mangeCard.add(directText);
		directText.setColumns(40);

		JButton chooseFileBtn = new JButton("Browse");
		chooseFileBtn.setBounds(349, 553, 89, 23);
		mangeCard.add(chooseFileBtn);

		JButton uploadBtn = new JButton("Upload");
		uploadBtn.setBounds(448, 553, 89, 23);
		mangeCard.add(uploadBtn);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(13, 37, 764, 380);
		mangeCard.add(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));

		String[] tableTitle = { "Name", "Type", "Version", "Created Time", "Modified Time", "Status" };
		table = new JTable();
		DefaultTableModel contactTableModel = (DefaultTableModel) table.getModel();
		contactTableModel.setColumnIdentifiers(tableTitle);
		panel_2.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), BorderLayout.CENTER);

		linkDriveText = new JTextField();
		linkDriveText.setBounds(86, 6, 253, 20);
		mangeCard.add(linkDriveText);
		linkDriveText.setColumns(10);

		JLabel lblLinkDrive = new JLabel("Link drive");
		lblLinkDrive.setBounds(13, 9, 68, 14);
		mangeCard.add(lblLinkDrive);

		JLabel lblNewLabel = new JLabel("Local Folder");
		lblNewLabel.setBounds(365, 9, 83, 14);
		mangeCard.add(lblNewLabel);

		localFolderText = new JTextField();
		localFolderText.setBounds(458, 6, 230, 20);
		mangeCard.add(localFolderText);
		localFolderText.setColumns(10);

		JButton localFolderBtn = new JButton("Browser");
		
		localFolderBtn.setBounds(688, 5, 89, 23);
		mangeCard.add(localFolderBtn);

		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(100);
		columnModel.getColumn(1).setPreferredWidth(20);
		columnModel.getColumn(2).setPreferredWidth(20);
		columnModel.getColumn(3).setPreferredWidth(200);
		columnModel.getColumn(4).setPreferredWidth(200);
		JButton manageBtn = new JButton("Manage");
		manageBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					layout.show(panel_1, "name_40465715806500");
					url = linkDriveText.getText();
//					table = new JTable(drive.getFile(url), tableTitle);
//					table.repaint();
					if (!url.equals("")) {
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						model.setRowCount(0);
						show_file(url);
					}
				} catch (GeneralSecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		manageBtn.setForeground(Color.WHITE);
		manageBtn.setBackground(Color.BLACK);
		manageBtn.setBounds(21, 126, 89, 23);
		panel.add(manageBtn);
		chooseFileBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				int r = j.showOpenDialog(null);
				if (r == JFileChooser.APPROVE_OPTION){
					directText.setText(j.getSelectedFile().getAbsolutePath());
				}else {
					directText.setText("the user cancelled the operation");
				}
			}
		});

		JPanel historyCard = new JPanel();
		historyCard.setBackground(Color.DARK_GRAY);
		panel_1.add(historyCard, "name_40467661985200");
		
		JPanel panel_3 = new JPanel();
		historyCard.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		String[] tableTitle1 = { "Name", "Method", "File name", "Time"};
		historyTable = new JTable();
		DefaultTableModel contactTableModel1 = (DefaultTableModel) historyTable.getModel();
		contactTableModel1.setColumnIdentifiers(tableTitle1);
		panel_3.add(new JScrollPane(historyTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), BorderLayout.CENTER);
		
		historyFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				layout.show(panel_1, "name_40467661985200");
				DefaultTableModel model = (DefaultTableModel) historyTable.getModel();
				model.setRowCount(0);
				try {
					show_history();
				} catch (GeneralSecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		JButton pushBtn = new JButton("Push");
		pushBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String localFolder = localFolderText.getText().toString();
				String[] listFilePush = drive.getListFile(localFolder);
				url = linkDriveText.getText();
				String folderId = drive.getFolderId(url);
				for(int i = 0; i < listFilePush.length; i++) {
					String linkFileLocal = localFolder+ "\\" + listFilePush[i];
					try {
						drive.uploadFile(linkFileLocal, folderId);
					} catch (GeneralSecurityException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		pushBtn.setBounds(13, 469, 89, 23);
		mangeCard.add(pushBtn);
		
		JButton pullBtn = new JButton("Pull");
		pullBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					drive.saveFile("1Ksi_fIt5F1U4XAZhWk5Cw6m-aEBbjeuhm473Xyp2R6Q", "C:\\Users\\Tran Phuong\\Desktop\\doc.txt");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (GeneralSecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		pullBtn.setBounds(134, 468, 97, 25);
		mangeCard.add(pullBtn);
		JTextPane notitext = new JTextPane();
		notitext.setBounds(391, 447, 238, 65);
		mangeCard.add(notitext);
		JButton btnCheck = new JButton("Check");
		btnCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					String path = localFolderText.getText().toString();
					String url = linkDriveText.getText().toString();
					try {
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						model.setRowCount(0);
						String[][] statusRender = drive.checkStatus(path, url);
						String noti = reRenderTable(url, statusRender);
						notitext.setText(noti);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (GeneralSecurityException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
			}
		});
		btnCheck.setBounds(253, 469, 89, 23);
		mangeCard.add(btnCheck);
		
	
		
		uploadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String directory = directText.getText();
				try {
					url = linkDriveText.getText();
					String folderId = drive.getFolderId(url);
					drive.uploadFile(directory, folderId);
				} catch (GeneralSecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		localFolderBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				int r = j.showOpenDialog(null);
				if (r == JFileChooser.APPROVE_OPTION){
					localFolderText.setText(j.getCurrentDirectory().getAbsolutePath());
				}else {
					localFolderText.setText("the user cancelled the operation");
				}
			}
		});
	}

	public void show_file(String url) throws GeneralSecurityException, IOException {
		String[][] listFile = drive.getFile(url);
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		Object[] data = new Object[5];
		for (int i = 0; i < listFile.length; i++) {
			data[0] = listFile[i][0];
			data[1] = listFile[i][1];
			data[2] = listFile[i][2];
			data[3] = listFile[i][3];
			data[4] = listFile[i][4];
			String comp = data[0] + " " + data[3] + " " + data[4];
//			System.err.println(comp);
			model.addRow(data);
		}
	}
	
	public String reRenderTable(String url, String[][] status) throws GeneralSecurityException, IOException {
		TableColumn tColumn;
		String[][] listFile = drive.getFile(url);
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		Object[] data = new Object[6];
		String noti = "";
		for (int i = 0; i < listFile.length; i++) {
			data[0] = listFile[i][0];
			data[1] = listFile[i][1];
			data[2] = listFile[i][2];
			data[3] = listFile[i][3];
			data[4] = listFile[i][4];
			System.out.println(listFile[i][0]);
			for(int j = 0; j < status.length; j++) {
				boolean check = true;
				if(listFile[i][0].equals(status[j][0])) {
					check = false;
					if(status[j][1].equals("1")) {
						data[5] = "New";
						
					}else if(status[j][1].equals("2")) {
						check = false;
						data[5] = "Old";
						noti+= data[0] + "\n";
					}
				}
				
			}
			model.addRow(data);
		}
		return noti;
	}
	
	public void show_history() throws GeneralSecurityException, IOException {
		String[][] listHistory = sheet.readSheet(drive.getHistoryId());
		System.out.println(listHistory.toString());
		DefaultTableModel model = (DefaultTableModel) historyTable.getModel();
		Object[] data = new Object[4];
		for (int i = 0; i < listHistory.length; i++) {
			data[0] = listHistory[i][0];
			data[1] = listHistory[i][1];
			data[2] = listHistory[i][2];
			data[3] = listHistory[i][3];
			System.out.println(data[0] + " " + data[1] + " " +data[2] + " " +data[3] );
			model.addRow(data);
		}
	}
	
	class ColumnColorRenderer extends DefaultTableCellRenderer {
		   Color backgroundColor, foregroundColor;
		   public ColumnColorRenderer(Color backgroundColor, Color foregroundColor) {
		      super();
		      this.backgroundColor = backgroundColor;
		      this.foregroundColor = foregroundColor;
		   }
		   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,   boolean hasFocus, int row, int column) {
		      Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		      cell.setForeground(foregroundColor);
		      return cell;
		   }
		}
}