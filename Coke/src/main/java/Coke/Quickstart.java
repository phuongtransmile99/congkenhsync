package Coke;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.googleapis.media.MediaHttpDownloaderProgressListener;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.Drive.About;
import com.google.api.services.drive.Drive.Files;
import model.FileItem;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

public class Quickstart {
	private static final String APPLICATION_NAME = "Google Drive API Java Quickstart";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";
	private String sheetHistoryId = "", sheetStatusId = "";

	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = Quickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

	public void uploadFile(String directory, String folderid) throws GeneralSecurityException, IOException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();
		File fileMetadata = new File();
		java.io.File filePath = new java.io.File(directory);
		Path path = Paths.get(directory);
		Path filename = path.getFileName();
		fileMetadata.setName(filename.toString());
		fileMetadata.setParents(Arrays.asList(folderid));
		FileContent mediaContent = new FileContent(fileMetadata.getMimeType(), filePath);
		File file = service.files().create(fileMetadata, mediaContent).setFields("id").execute();

		com.google.api.services.drive.model.About about = service.about().get().setFields("user").execute();
		GoogleSheet sheet = new GoogleSheet();
		String name = about.getUser().getDisplayName();
		String pattern = " HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());
		sheet.writeHistory(sheetHistoryId, name, "Upload", filename.toString(), date);
//		System.out.println("File ID: " + file.getId());
	}

	public List<FileItem> listFile(String folderId) throws GeneralSecurityException, IOException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Drive drive = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();
		List<FileItem> responseList = new ArrayList<>();
		FileList fileList = drive.files().list()
				.setFields("files(kind, id, name, version, createdTime, modifiedTime, size, parents)").execute();

		List<String> type;
		for (File file : fileList.getFiles()) {
			if (file.getParents() != null && file.getParents().toString().equals("[" + folderId + "]")) {
				FileItem f = new FileItem();
				f.setId(file.getId());
				f.setName(file.getName());
				f.setVersion(file.getVersion());
				f.setCreatedTime(file.getCreatedTime());
				f.setModifiedTime(file.getModifiedTime());
				if (file.getSize() == null) {
					f.setType("Folder");
				} else {
					f.setType("File");
				}
				String s = "";
				if (file.getParents() != null) {
					s = file.getParents().toString();
				}
//				System.out.println(file.getKind() + ": " + file.getName() + " " + file.getId() + " ---- " + s);
				responseList.add(f);
			}
		}
		initSheet(responseList);
		move(folderId, sheetHistoryId);
		move(folderId, sheetStatusId);
		return responseList;
	}

	public void logHistory(String userName, String nameFile, DateTime editDate, String method) {

	}

	public String[][] getFile(String url) throws GeneralSecurityException, IOException {
		StringTokenizer stk = new StringTokenizer(url, "/");
		String folderId = "";
		while (stk.hasMoreTokens()) {
			folderId = stk.nextToken();
		}
		List<FileItem> listFile = listFile(folderId);
		int length = listFile.size();
		String[][] data = new String[length][5];
		for (int i = 0; i < length; i++) {
			data[i][0] = listFile.get(i).getName();
			data[i][1] = listFile.get(i).getType();
			data[i][2] = Long.toString(listFile.get(i).getVersion());
			data[i][3] = formatDate(listFile.get(i).getCreatedTime());
			data[i][4] = formatDate(listFile.get(i).getModifiedTime());
//			System.out.println(data[i][0] + " " + data[i][1] + " " + data[i][2] + " " + data[i][3] + " " + data[i][4]);
		}
		return data;
	}

	public void deleteFile(String fileId) throws GeneralSecurityException, IOException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();

		service.files().delete(fileId);

	}

	public String[] getListFile(String directory) {
		java.io.File directoryPath = new java.io.File(directory);
		// List of all files and directories
		String contents[] = directoryPath.list();
		System.out.println("List of files and directories in the specified directory:");
		for (int i = 0; i < contents.length; i++) {
//			System.out.println(contents[i]);
		}
		return contents;
	}

	public String getFolderId(String url) {
		StringTokenizer stk = new StringTokenizer(url, "/");
		String folderId = "";
		while (stk.hasMoreTokens()) {
			folderId = stk.nextToken();
		}
		return folderId;
	}

	public String formatDate(DateTime date) {
		String input = date.toString();
		return input.substring(0, 10) + " " + input.substring(11, 19);
	}

	class CustomProgressListener implements MediaHttpDownloaderProgressListener {
		public void progressChanged(MediaHttpDownloader downloader) {
			switch (downloader.getDownloadState()) {
			case MEDIA_IN_PROGRESS:
//				System.out.println("AAA");
				System.out.println(downloader.getProgress());
				break;
			case MEDIA_COMPLETE:
				System.out.println("Download is complete!");
			}
		}
	}

	public void saveFile(String fileId, String directory) throws IOException, GeneralSecurityException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();

		fileId = "1Ksi_fIt5F1U4XAZhWk5Cw6m-aEBbjeuhm473Xyp2R6Q";
		OutputStream outputStream = new ByteArrayOutputStream();
		service.files().get(fileId).executeMediaAndDownloadTo(outputStream);
	}

	public void initSheet(List<FileItem> listFile) throws IOException, GeneralSecurityException {
		boolean check1 = false, check2 = false;
		GoogleSheet sheet = new GoogleSheet();
		for (FileItem f : listFile) {
//			System.out.println(f.getName());
			if (f.getName().equals("History")) {
				check1 = true;
//				System.out.println(check1);
				sheetHistoryId = f.getId();
			}
			if (f.getName().equals("Status")) {
				check2 = true;
//				System.out.println(check2);
				sheetStatusId = f.getId();
			}
		}
		if (check1 == false) {
			sheetHistoryId = sheet.create("History");
		}
		if (check2 == false) {
			sheetStatusId = sheet.create("Status");
		}
		System.out.println("Check: " + sheetHistoryId);
	}

	public void move(String folderId, String fileId) throws IOException, GeneralSecurityException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();
		File file = service.files().get(fileId).setFields("parents").execute();
		StringBuilder previousParents = new StringBuilder();
		for (String parent : file.getParents()) {
			previousParents.append(parent);
			previousParents.append(',');
		}
		// Move the file to the new folder
		file = service.files().update(fileId, null).setAddParents(folderId).setRemoveParents(previousParents.toString())
				.setFields("id, parents").execute();
	}

//	public void checkFile() throws GeneralSecurityException, IOException {
//		System.out.println(sheetHistoryId);
//		GoogleSheet sheet = new GoogleSheet();
//		sheet.readSheet(sheetHistoryId);
//	}
	public String getHistoryId() {
		System.out.println("check get :" + sheetHistoryId);
		return sheetHistoryId;
	}

	public String getStatusId() {
		return sheetStatusId;
	}

	public String[][] getListFileLocal(String path) throws IOException {
		java.io.File f = new java.io.File(path);
		int i = 0;
		String[] listFile = f.list();
		String[][] result = new String[listFile.length][3];
		for (String fileName : listFile) {

			java.io.File fi = new java.io.File(path + "\\" + fileName);
			String pattern = "yyyy-MM-dd HH:mm:ss";
			BasicFileAttributes attrs;
			attrs = java.nio.file.Files.readAttributes(fi.toPath(), BasicFileAttributes.class);

			FileTime time = attrs.creationTime();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String createdTime = simpleDateFormat.format(new Date(time.toMillis()));

			FileTime time2 = attrs.lastModifiedTime();
			SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern);
			String modifiedTime = simpleDateFormat1.format(new Date(time2.toMillis()));

			result[i][0] = fileName;
			result[i][1] = createdTime;
			result[i][2] = modifiedTime;
			i++;
		}
		return result;
	}

	public String[][] checkStatus(String path, String url)
			throws IOException, GeneralSecurityException, ParseException {
		String[][] listFileLocal = getListFileLocal(path);
		String[][] listFileDrive = getFile(url);
		String[][] statusReturn = new String[listFileLocal.length][2];
		// 1 la moi hon, 2 la cu hon, 0 la bang nhau
		for (int i = 0; i < listFileLocal.length; i++) {
			for (int j = 0; j < listFileDrive.length; j++) {
				if (listFileLocal[i][0].equals(listFileDrive[j][0])) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date date1 = sdf.parse(listFileLocal[i][2]);
					Date date2 = sdf.parse(listFileDrive[j][4]);
					if (date1.compareTo(date2) > 0) {
						statusReturn[i][0] = listFileLocal[i][0];
						statusReturn[i][1] = "1";
					} else if (date1.compareTo(date2) < 0) {
						statusReturn[i][0] = listFileLocal[i][0];
						statusReturn[i][1] = "2";
					} else if (date1.compareTo(date2) == 0) {
						statusReturn[i][0] = listFileLocal[i][0];
						statusReturn[i][1] = "0";
					}
				}
			}
		}
		return statusReturn;
	}

}
