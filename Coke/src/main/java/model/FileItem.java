package model;

import java.util.Date;

import com.google.api.client.util.DateTime;

public class FileItem {
	private String id;
	private String name;
	private long version;
	private DateTime createdTime;
	private DateTime modifiedTime;
	private String type;
	public String getId() {
		return id;
	}
	public DateTime getCreatedTime() {
		return createdTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setCreatedTime(DateTime dateTime) {
		this.createdTime = dateTime;
	}
	public DateTime getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(DateTime modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}

}
