package model;

public class SheetId {
	private String historyId;
	private String statusId;
	public String getHistoryId() {
		return historyId;
	}
	public void setHistoryId(String historyId) {
		this.historyId = historyId;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public SheetId(String historyId, String statusId) {
		super();
		this.historyId = historyId;
		this.statusId = statusId;
	}

}
