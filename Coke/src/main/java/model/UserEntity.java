package model;

public class UserEntity {
	private String name;
	private String action;
	private String time;
	private String fileName;
	private String fileId;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public UserEntity(String name, String action, String time, String fileName, String fileId) {
		super();
		this.name = name;
		this.action = action;
		this.time = time;
		this.fileName = fileName;
		this.fileId = fileId;
	}
	
}
